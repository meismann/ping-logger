class PingLogger < Struct.new(:server)
  PING_TIME_LIMIT = 60

  def run!
    puts "Starting ping loops on #{server}…"
    while !@exit_on_loop_end do
      @ping_cmd_start_timestamp = Time.now.to_i
      append_to_file parse `ping -t #{PING_TIME_LIMIT} #{server}`
    end
  end

  def stop!
    p "Stopping loop of #{server}"
    @exit_on_loop_end = true
  end

  private

  def append_to_file(str)
    Thread.new do
      File.open(filename, 'a') do |csv_file|
        csv_file.write str
      end
    end
  end

  def filename
    @filename ||= "#{server}@#{Time.now.strftime('%F_%Hh%M.%S')}.csv"
  end

  def parse(ping_output)
    ping_output
      .split("\n")
      .select{ |line| line =~ /bytes from|Request timeout/ }
      .map{ |line| line.sub(/Request timeout for icmp_seq (\d+)/, '\1,Timeout') }
      .map{ |line| line.sub(/^.+icmp_seq=(\d+)\s.+time=([\d.]+)\sms/, '\1,\2') }
      .map{ |csv_line| csv_line.sub(/^\d+/, &method(:timestamp_from)) }
      .join("\n") + "\n"
  end

  def timestamp_from(icmp_seq)
    icmp_seq.to_i + @ping_cmd_start_timestamp
  end
end

ping_loggers = []
trap('INT') do 
  ping_loggers.each &:stop!
end

SERVERS = ARGV.pop.split(',')
SERVERS.map do |server|
  Thread.new do
    ping_loggers << ping_logger = PingLogger.new(server)
    ping_logger.run!
  end
end .each &:join
