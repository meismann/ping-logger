require 'SVG/Graph/DataPoint'
require 'SVG/Graph/Line'

class Graph < Struct.new(:timeseries)
  FILENAME = 'line.svg'
  X_AXIS_INCREMENTS_SECS = 60

  def draw!
    graph = SVG::Graph::Line.new(options)
    timeseries.each do |title, data_hash|
      graph.add_data(data: data_hash.values, title: title)
    end

    File.write(FILENAME, graph.burn)
    p "Written to #{FILENAME}!"
  end

  private

  def options 
    {
      :width             => width,
      :height            => 800,
      :fields            => x_axis_humanized,
      :graph_title       => graph_title,
      :show_graph_title  => true,
      :x_title           => 'Messzeit',
      :show_x_title      => true,
      :y_title           => 'Ping-Zeit in ms',
      :show_y_title      => true,
      :show_data_points  => false,
      :show_data_values  => false, # process time and readability!
      :no_css            => true,
      :x_axis_position   => 0,
      key_position: :left
    }
  end

  def x_axis
    @x_axis ||= timeseries.values.map(&:keys).flatten.uniq
  end

  def x_axis_humanized
    x_axis.map do |timestamp| 
      timestamp % X_AXIS_INCREMENTS_SECS != 0 ? '' : 
        Time.at(timestamp).strftime("%Hh%M'%S")
    end
  end

  def graph_title
    "Ping-Zeiten zwischen #{humanize(x_axis.first)} und #{humanize(x_axis.last)}"
  end

  def humanize(timestamp)
    Time.at(timestamp).strftime('%A, %-d. %B %Y %Hh%M')
  end

  def width
    [300, x_axis.length * 2].max
  end
end

class TimeseriesParser < Struct.new(:filename)
  VALUE_AT_FAILURE = -200
  Y_VALUES_UPPER_LIMIT = 1000

  def self.with_unify_domains(timeseries)
    unified_domain = Range.new(*timeseries.values.map(&:keys).flatten.minmax.sort).to_a
    Hash[timeseries.map do |name, timeseries|
      ext_timeseries = Hash[unified_domain.map do |timestamp|
        [timestamp, timeseries[timestamp] || VALUE_AT_FAILURE]
      end]
      [name, ext_timeseries]
    end]
  end

  def parse
    lines = File
      .read(filename)
      .split("\n")
      .reject(&:empty?)
      .map!{ |line| line.split(',').map(&method(:parse_number)) }
      .each{ |pair| pair[1] = [pair[1], Y_VALUES_UPPER_LIMIT].min }
      .sort!
    Hash[lines]
  end

  private

  def parse_number(str)
    return VALUE_AT_FAILURE unless str =~ /\d/
    return str.to_f if str =~ /\./
    str.to_i
  end
end

timeseries = Hash[ARGV.map do |filename|
  [filename.split('@').first, TimeseriesParser.new(filename).parse]
end]

timeseries = TimeseriesParser.with_unify_domains(timeseries)

Graph.new(timeseries)
  .draw!
